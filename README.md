# Portfolio for Mad Dev

[Live View of my Portfolio](https://portfolio-mad-dev-react.onrender.com/)
## This project is a showcase example of my frontend technologies.

My Frontend technologies include

1. HTML
2. CSS
3. Javascript
4. Typescript
5. React.js
6. Next.js
7. Vue.js
8. Angular

## Note that I am also an expert in backend tech stack.

My Backend technologies include

1. PHP(Laravel, Symfony, Wordpress, CodeIgniter ...)
2. Node.js(Express.js, Nest.js)
3. Python(Django, Scrapy)
4. Ruby(Ruby on Rails)
5. MySQL
6. MongoDB
7. PostgreSQL

## Master of Tools, DevOps Expert.

- Apache, nginx, tomcat
- Vscode
- Docker
- Git
- CI/CD (Git + Github + render.com)
- SSH server & Client tools


